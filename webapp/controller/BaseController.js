sap.ui.define([
		"sap/ui/core/mvc/Controller"
	], function (Controller) {
		"use strict";
//every controller has a path to base controller like this : "znkrishsapui5app/controller/BaseController"
//and each controller returns a base controller 
//e.g.return BaseController.extend("znkrishsapui5app.controller.MainScreen"
		return Controller.extend("znkrishsapui5app.controller.BaseController", {
		//if we want to use the below methods across controllers we just need to say this.
		//e.g. this.getModel
			getRouter : function () {
				return sap.ui.core.UIComponent.getRouterFor(this);
			},

		
			getModel : function (sName) {
				return this.getView().getModel(sName);
			},

		
			setModel : function (oModel, sName) {
				return this.getView().setModel(oModel, sName);
			},

		
			getResourceBundle : function () {
				return this.getOwnerComponent().getModel("i18n").getResourceBundle();
			}

		});

	}
);