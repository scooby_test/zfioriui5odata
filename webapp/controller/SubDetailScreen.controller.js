sap.ui.define([
	"znkrishsapui5app/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("znkrishsapui5app.controller.SubDetailScreen", {

   
		onInit: function () {
			this.getRouter().getRoute("SubDetailScreen").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {
			
		}
 
	});

});