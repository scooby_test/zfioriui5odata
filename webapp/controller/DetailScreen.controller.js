/*/*global location*/
sap.ui.define([
	"znkrishsapui5app/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"znkrishsapui5app/model/formatter"
], function (
	BaseController,
	JSONModel,
	History,
	formatter
) {
	"use strict";

	return BaseController.extend("znkrishsapui5app.controller.DetailScreen", {

		formatter: formatter,

		onInit: function () {
			// get the router of this view , attachpatternmatched ( pattern name from manifest.json)
			// match the pattern of this view. this.<anymethodname> here it is _onObjectMatched, 
			// this is for the current object 
			this.getRouter().getRoute("DetailScreen").attachPatternMatched(this._onObjectMatched, this);
			this.oModel = this.getOwnerComponent().getModel();
		},

		handleNext: function () {
			this.getRouter().navTo("SubDetailScreen");
		},
		handleBack: function () {
			this.getRouter().navTo("MainScreen");
		},

		_onObjectMatched: function (oEvent) {
			that = this;
			//this is the method to get the data from target view,
			// this is the destination view in this scenario
			//parameters defined in the manifest.json
			var Parameters = oEvent.getParameters().arguments;
			var empid = Parameters.empid;
			var path = "/EMP_INFOSet('" + empid + "')";
			//read table - get entity from the backend instead of the details from previous screen
			//thats why you dont need the Odata.results like we did in the Main screen
			that.oModel.read(path, {
				async: true,
				success: function (oData) {
					that.byId("inpempid").setValue(oData.Empid);
					that.byId("inpfullname").setValue(oData.Fullname);
					that.byId("inpemailid").setValue(oData.Emailid);
					that.byId("inpdesignation").setValue(oData.Designation);
					that.byId("inpdoj").setValue(oData.Doj);
				},
				error: function (oError) {}
			});
		}

	});

});