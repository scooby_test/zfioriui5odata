var that;
sap.ui.define([
	"znkrishsapui5app/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"znkrishsapui5app/model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator, MessageBox) {
	"use strict";

	return BaseController.extend("znkrishsapui5app.controller.MainScreen", {

		formatter: formatter,
		onInit: function () {

			//if the variable for fragment has not been defined then execute the if 
			if (!this.oDialog) {
				// if you create a variable with var it is local to a function 
				//to create a variable which is available across function we use this
				//this after the path is a keyword , refers to current object 
				this.oDialog = sap.ui.xmlfragment("znkrishsapui5app.fragments.Dialog", this);
			}
			//to add i18n for fragment we need to add the below code 
			//this.view is the current view for this controller
			//add dependent files = i18n is a dependent component
			// for the fragment add the dependent files to the view 
			//so that it can be initialised
			this.getView().addDependent(this.oDialog);
			that = this;
			//model for combobox
			var json = new JSONModel();
			var cmbdesig = this.byId("cmbdesig");
			var comboData = [{
				"designation": "Consultant"
			}, {
				"designation": "Senior Consultant"
			}, {
				"designation": "Manager"
			}, {
				"designation": "Director"
			}];
			json.setData({
				'cmbData': comboData //importing custom data
			});
			//setting json to sapui5 control
			cmbdesig.setModel(json);
			
			//creating all variables at one place 
			// this is only for OData model
			//this.oModel = this.getOwnerComponent().getModel();
			this.empid = this.byId("inpempid");
			this.fullname = this.byId("inpfullname");
			this.emailid = this.byId("inpemailid");
			this.desig = this.byId("cmbdesig");
			this.doj = this.byId("doj");
			//adding texts in i18n
			this.deleteconfirm = this.getResourceBundle().getText("deleteconfirm");
			this.count = this.getResourceBundle().getText("count");
			//bind table
			this.bindTable();
		},
		initViewMode:function()
		{
			//holding control on model 
			var viewmodel = new JSONModel();
		},
		
		bindTable: function () {
			that = this;
			//manifest has the service url child of owner component 
			var oModel = this.getOwnerComponent().getModel();
			//name of entityset
			var path = "/EMP_INFOSet";
			//read table - get entityset
			oModel.read(path, {
				async: true,
				success: function (oData) {
					var datalength = oData.results.length;
					//get the resource bundle and then concatenate with staic text
					//count is defined in the i18N 
					that.byId('lblCount').setText(that.count + "(" + [datalength] + ")");
					var oTab = that.byId("oTable");
					var json = new JSONModel();
					json.setData({
						'modelData': oData.results
					});
					oTab.setModel(json);
				},
				error: function (oError) {}
			});
		},
		resetAll: function () {
			this.byId("inpempid").setValue("");
			this.byId("inpfullname").setValue("");
			this.byId("inpemailid").setValue("");
			this.byId("cmbdesig").setValue("");
			this.byId("doj").setValue("");
		},
		handleNext: function () {
			//e.g this.getRouter ( defined in base controller)
			//for setting messagebox as an example for i18n
			//var msgbox=this.getResourceBundle().getText("<keyname from i18n>");
			//MessageBox.show(msgbox,{title:"Information",icon:MessageBox.Icon.INFORMATION})
			// to navigate from one screen to another 
			// 1) this.getRouter() => this has been initialised in basecontroller
			// 2) this.getRouter().navTo => navigate where ?
			// 3) get the name of view name from the manifest.json from routes 
			// you will get the target => from target get the view name
			// 4) give the view name ( copy name from manifest so that no errors)
			/*		var empid = this.byId("inpempid").getValue();
					var fullname = this.byId("inpfullname").getValue()
					var emailid = this.byId("inpemailid").getValue();
					this.getRouter().navTo("DetailScreen", {
						//variables from manifest.json
						"empid": empid,
						"fullname": fullname,
						"emailid": emailid
					});*/
		},

		handleEdit: function (oEvent) {
			//showing the data on click of edit
			//1) button=> getParent => hbox
			//2) HBox=>getParent => items
			//3) 
			var oParent = oEvent.getSource().getParent().getParent();
			var oCore = sap.ui.getCore();
			//horizontal cells will always remain same
			//only vertical will change
			var empid = oParent.getCells()[0].getText();
			var fullname = oParent.getCells()[1].getText();
			var emailid = oParent.getCells()[2].getText();
			var designation = oParent.getCells()[3].getText();
			var doj = oParent.getCells()[4].getText();
			oCore.byId("inpfragempid").setValue(empid);
			oCore.byId("inpfragfullname").setValue(fullname);
			oCore.byId("inpfragemailid").setValue(emailid);
			oCore.byId("cmbfragdesig").setValue(designation);
			oCore.byId("fragdoj").setValue(doj);
			this.oDialog.open();
		},

		handleDisplay: function (oEvent) {
			//parameter with query
			//oEvent can be any name
			// get cells acts as an array /array has an index
			//we will use get cells as a method 
			//and then pass the index for each of the fields
			//as we are using the button on the row we need to get the parent of button 
			// then the parent of hbox
			var oParent = oEvent.getSource().getParent().getParent();
			var empid = oParent.getCells()[0].getText();
		/*	var fullname = oParent.getCells()[1].getText();
			var emailid = oParent.getCells()[2].getText();
			var designation = oParent.getCells()[3].getText();
			var doj = oParent.getCells()[4].getText();*/
			this.byId("inpempid").setValue(empid);
		/*	this.byId("inpfullname").setValue(fullname);
			this.byId("inpemailid").setValue(emailid);
			this.byId("cmbdesig").setValue(designation);
			this.byId("doj").setValue(doj);*/
			//e.g this.getRouter ( defined in base controller)
			//for setting messagebox as an example for i18n
			//var msgbox=this.getResourceBundle().getText("<keyname from i18n>");
			//MessageBox.show(msgbox,{title:"Information",icon:MessageBox.Icon.INFORMATION})
			// to navigate from one screen to another 
			// 1) this.getRouter() => this has been initialised in basecontroller
			// 2) this.getRouter().navTo => navigate where ?
			// 3) get the name of view name from the manifest.json from routes 
			// you will get the target => from target get the view name
			// 4) give the view name ( copy name from manifest so that no errors)
			this.getRouter().navTo("DetailScreen", {
				//variables from manifest.json
				//names should exactly match
				"empid": empid
			/*	"fullname": fullname,
				"emailid": emailid,
				"desig": designation,
				"doj": doj*/
			});
			return;
		},

		handleDelete: function (oEvent) {
			var oParent = oEvent.getSource().getParent().getParent();
			var empid = oParent.getCells()[0].getText();
			// need to add this (as we are referring to the oModel in the messagebox)

			//name of entityset
			//to add single quote ( '""')
			//delete requires the empid 
			var path = "/EMP_INFOSet('" + empid + "')";
			MessageBox.show(this.deleteconfirm, {
				title: "Question",
				icon: MessageBox.Icon.QUESTION,
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				onClose: function (oAction) {
					if (oAction === "YES") {
						that.oModel.remove(path, {
							async: true,
							success: function (oData) {
								MessageBox.show("Employee record deleted successfully", {
									title: "Information",
									icon: MessageBox.Icon.SUCCESS,
									onClose: function () {
										//rebind to the screen
										that.bindTable();
									}
								});
							},
							error: function (oError) {}
						});
					} else if (oAction === "NO") {
						//do Nothing
					}
				}
			});

		},
		handleClose: function () {
			this.oDialog.close();
		},

		handleUpdate: function () {
			var oCore = sap.ui.getCore();
			var fullname = oCore.byId("inpfragfullname");
			var emailid = oCore.byId("inpfragemailid");
			var desig = oCore.byId("cmbfragdesig");
			var doj = oCore.byId("fragdoj");
			if (fullname.getValue() === "" || emailid.getValue() === "" || desig.getValue() === "" || doj.getValue() === "") {
				MessageBox.show("Please enter all the mandatory fields", {
					title: "Warning",
					icon: MessageBox.Icon.ERROR
				});
				//return will not execute the lines after it
				return;
			}
			var data = {};
			var empid = oCore.byId("inpfragempid");
			data.Fullname = fullname.getValue();
			data.Emailid = emailid.getValue();
			data.Designation = desig.getValue();
			data.Doj = doj.getValue();

			//var oModel = this.getOwnerComponent().getModel();
			//name of entityset
			//to add single quote ( '""')
			//update requires the empid 
			var path = "/EMP_INFOSet('" + empid.getValue() + "')";
			that.oModel.update(path, data, {
				async: true,
				success: function (oData) {
					MessageBox.show("Employee record updated successfully", {
						title: "Information",
						icon: MessageBox.Icon.SUCCESS,
						//rebind the table with new entries
						onClose: function () {
							//rebind to the screen
							that.bindTable();
							//close dialog
							that.oDialog.close();
						}
					});
				},
				error: function (oError) {}
			});

		},
		handleSave: function () {
			//create record in backend and save

			if (this.fullname.getValue() === "" || this.emailid.getValue() === "" || this.desig.getValue() === "" || this.doj.getValue() ===
				"") {
				MessageBox.show("Please enter all the required fields", {
					title: "Warning",
					icon: MessageBox.Icon.ERROR
				});
				//return will not execute the lines after it
				return;
			}
			//can follow any of the two approaches
			//approach 1
			// var data = {
			// 	"Empid": empid.getValue(),
			// 	"Fullname": fullname.getValue(),
			// 	"Emailid": emailid.getValue(),
			// 	"Designation": desig.getValue(),
			// 	"Doj": doj.getValue(),
			// };
			//approach 2
			var data = {};
			data.Empid = this.empid.getValue();
			data.Fullname = this.fullname.getValue();
			data.Emailid = this.emailid.getValue();
			data.Designation = this.desig.getValue();
			data.Doj = this.doj.getValue();

			//var oModel = this.getOwnerComponent().getModel();
			//name of entityset
			var path = "/EMP_INFOSet";
			this.oModel.create(path, data, {
				async: true,
				success: function (oData) {
					MessageBox.show("Employee record created successfully", {
						title: "Information",
						icon: MessageBox.Icon.SUCCESS,
						//rebind the table with new entries
						onClose: function () {
							//rebind to the screen
							that.bindTable();
							//reset the values 
							that.resetAll();
						}
					});
				},
				error: function (oError) {}
			});
		},

		handleRowSelect: function (oEvent) {
			//parameter with query
			//oEvent can be any name
			// get cells acts as an array /array has an index
			//we will use get cells as a method 
			//and then pass the index for each of the fields
			var empid = oEvent.getSource().getCells()[0].getText();
			var fullname = oEvent.getSource().getCells()[1].getText();
			var emailid = oEvent.getSource().getCells()[2].getText();
			var designation = oEvent.getSource().getCells()[3].getText();
			var doj = oEvent.getSource().getCells()[4].getText();
			this.byId("inpempid").setValue(empid);
			this.byId("inpfullname").setValue(fullname);
			this.byId("inpemailid").setValue(emailid);
			this.byId("cmbdesig").setValue(designation);
			this.byId("doj").setValue(doj);
			//e.g this.getRouter ( defined in base controller)
			//for setting messagebox as an example for i18n
			//var msgbox=this.getResourceBundle().getText("<keyname from i18n>");
			//MessageBox.show(msgbox,{title:"Information",icon:MessageBox.Icon.INFORMATION})
			// to navigate from one screen to another 
			// 1) this.getRouter() => this has been initialised in basecontroller
			// 2) this.getRouter().navTo => navigate where ?
			// 3) get the name of view name from the manifest.json from routes 
			// you will get the target => from target get the view name
			// 4) give the view name ( copy name from manifest so that no errors)
			this.getRouter().navTo("DetailScreen", {
				//variables from manifest.json
				//names should exactly match
				"empid": empid,
				"fullname": fullname,
				"emailid": emailid,
				"desig": designation,
				"doj": doj
			});
			return;
		}

	});
});